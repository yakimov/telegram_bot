#!/usr/bin/python
import config #файл с настройками
import telegram
import os
import subprocess
import sys
import shlex
import time
import datetime
from subprocess import Popen, PIPE
from telegram.ext import CommandHandler
from imp import reload #модуль для перезагрузки (обновления) других модулей

#bot = telegram.Bot(token = config.token)
#Проверка бота
#print(bot.getMe())
from telegram.ext import Updater
REQUEST_KWARGS={
    'proxy_url': config.proxy_url,
    # Optional, if you need authentication:
    'urllib3_proxy_kwargs': {
        'username': config.proxy_user,
        'password': config.proxy_pass,
    }
}
updater = Updater(token=config.token, request_kwargs=REQUEST_KWARGS)
dispatcher = updater.dispatcher

#выполнение команды shell и вывод результата в телеграмм
def run_command(command):
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    global textoutput
    textoutput = ''
    while True:
        global output
        output = process.stdout.readline()
        output = output.decode('utf8')
        if output == '' and process.poll() is not None:
            break
        if output:
            print (output.strip())
        textoutput = textoutput + '\n' + output.strip()
    rc = process.poll()
    return rc
    
#функция команады старт
def start(bot, update):
    kb = [[telegram.KeyboardButton('/getkoi'), telegram.KeyboardButton('/help')]]
    kb_markup = telegram.ReplyKeyboardMarkup(kb, resize_keyboard=True)
    bot.sendMessage(chat_id=update.message.chat_id, text="Hi there! Why are you here?", reply_markup=kb_markup)

#функция команады help
def help(bot, update):
    reload(config)
    reply_markup = telegram.ReplyKeyboardRemove()
    bot.sendMessage(chat_id=update.message.chat_id, text='''список доступных команд: 
    /id - id пользователя
    /ftp_tree - Get ftp-server tree
    /ifconfig - сетевые настройки
    /df - информация о дисковом пространстве (df -h)
    /free - информация о памяти
    /mpstat - информация о нагрузке на процессор
    /dir1 - объем папки''' + config.dir1 + '''
    /getkoi - получить кадр с камеры
    ''', reply_markup=reply_markup)

#Получить кадр из видеопоследовательности
def camShot(bot, update):
    reload(config) 
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
         run_command("ffmpeg -y -ss 00:00:01 -i rtsp://locus:@192.168.88.238/ -vframes 1 -q:v 2 "+config.dir_self+"output.jpg")
         # Create virtual keyboard
         kb = [[telegram.KeyboardButton('/getkoi'), telegram.KeyboardButton('/help')]]
         kb_markup = telegram.ReplyKeyboardMarkup(kb, resize_keyboard=True)
         # Send notification to bot owner
         if user != config.admin[0]:
             bot.sendMessage(chat_id=config.admin[0], text="The user with id "+user+" just got camshot from KOI")
         # Send info about the cmshot file
         bot.sendMessage(chat_id=update.message.chat_id, text="Cam stream from KOI at"+datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S %d.%m.%Y')+":") #+config.dir_self, reply_markup=kb_markup)
         # Send camshot itself
         bot.send_photo(chat_id=update.message.chat_id, photo=open(config.dir_self+ 'output.jpg', 'rb'))
    else:
         kb = [[telegram.KeyboardButton('/getkoi'), telegram.KeyboardButton('/help')]]
         kb_markup = telegram.ReplyKeyboardMarkup(kb, resize_keyboard=True)
         bot.sendMessage(chat_id=update.message.chat_id, text="Access denied! Please request the admin for access")
         bot.sendMessage(chat_id=config.admin[0], text="User with id "+user+" failed to get access to KOI camera")


#функция команады id
def myid(bot, update):
    userid = update.message.from_user.id
    chatid = update.message.chat_id
    bot.sendMessage(chat_id=update.message.chat_id, text="User id: "+str(userid) +'\n'+ "Chat_id: " + str(chatid))
    

#функция команады ftp_tree
def test_get_tree(bot, update):
    reload(config) 
    user = str(update.message.from_user.id)
    #if user in config.admin: #если пользовательский id в списке admin то команда выполняется
    log_pass_host = update.message.text.split(' ')[1]
    bot.sendMessage(chat_id=update.message.chat_id, text = "The ftp://" + log_pass_host + "/ tree is:")
    run_command("lftp ftp://" + log_pass_host +"/ -e 'du -ah;exit'")
    bot.sendMessage(chat_id=update.message.chat_id, text = textoutput)

#функция команады ifconfig
def ifconfig(bot, update):
    reload(config) 
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("ifconfig")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады df
def df(bot, update):
    reload(config) 
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("df -h")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады free
def free(bot, update):
    reload(config) 
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("free -m")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады mpstat
def mpstat(bot, update):
    reload(config) 
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        run_command("mpstat")
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)

#функция команады dir1
def dir1(bot, update):
    reload(config) 
    user = str(update.message.from_user.id)
    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
        dir1_command = "du -sh "+ config.dir1
        run_command(dir1_command)
        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)


        
#функция команады dirbackup - проверяет наличие файла по дате
#def dirbackup(bot, update):
#    reload(config) 
#    user = str(update.message.from_user.id)
#    if user in config.admin: #если пользовательский id в списке admin то команда выполняется
#        now_date = datetime.date.today() # Текущая дата
#        cur_year = str(now_date.year) # Год текущий
#        cur_month = now_date.month # Месяц текущий
#        if cur_month < 10:
#            cur_month = str(now_date.month)
#            cur_month = '0'+ cur_month
#        else:
#            cur_month = str(now_date.month)
#        cur_day = str(now_date.day) # День текущий
#        filebackup = config.dir_backup + cur_year + '-' + cur_month + '-' + cur_day + '.03.00.co.7z'  #формируем имя файла для поиска
#        print (filebackup)
#        filebackup_command = "ls -lh "+ filebackup
#        run_command(filebackup_command)
#        bot.sendMessage(chat_id=update.message.chat_id, text=textoutput)
        

    
    
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

camShot_handler = CommandHandler('getkoi', camShot)
dispatcher.add_handler(camShot_handler)

test_get_tree_handler = CommandHandler('ftp_tree', test_get_tree)
dispatcher.add_handler(test_get_tree_handler)

ifconfig_handler = CommandHandler('ifconfig', ifconfig)
dispatcher.add_handler(ifconfig_handler)

df_handler = CommandHandler('df', df)
dispatcher.add_handler(df_handler)

free_handler = CommandHandler('free', free)
dispatcher.add_handler(free_handler)

mpstat_handler = CommandHandler('mpstat', mpstat)
dispatcher.add_handler(mpstat_handler)

dir1_handler = CommandHandler('dir1', dir1)
dispatcher.add_handler(dir1_handler)

#dirbackup_handler = CommandHandler('dirbackup', dirbackup)
#dispatcher.add_handler(dirbackup_handler)

myid_handler = CommandHandler('id', myid)
dispatcher.add_handler(myid_handler)

help_handler = CommandHandler('help', help)
dispatcher.add_handler(help_handler)


updater.start_polling()
